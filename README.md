#Firebase_sign_in 

Soacial singin package lets you all necessary methods to your Flutter app.

## How to integrate in android 

Step 1: Configure Firebase project.
Go to the Firebase console (https://console.firebase.google.com/) and create a new Firebase project or use an existing project.

Add an Android app to your Firebase project and follow the instructions to download the 'google-services.json' file.
Place the 'google-services.json' file in the android/app directory of your Flutter project.

Step 2: Configure Android app
Open the android/build.gradle file and add the following under the dependencies section:
```build.gradle
classpath 'com.google.gms:google-services:4.3.10'
```
Open the android/app/build.gradle file and add the following at the bottom:

```app/build.gradle
apply plugin: 'com.google.gms.google-services'
```
Step 3: Firebase initialized in your main function.

Step 4: Implement the sign-in logic

## How to integrate in iOS

Step 1: Configure Firebase project

Go to the Firebase console (https://console.firebase.google.com/) and create a new Firebase project or use an existing project.

Add an iOS app to your Firebase project and follow the instructions to download the 'GoogleService-Info.plist' file.

Place the 'GoogleService-Info.plist' file in the ios/Runner directory of your Flutter project.

Step 2: Configure iOS app

Open Xcode and navigate to your Flutter project's ios/Runner directory. Open the Runner.xcworkspace file.

In Xcode, select the "Runner" project in the project navigator on the left. Then, select the "Runner" target and navigate to the "Signing & Capabilities" tab.

Enable the "Sign in with Apple" capability if it's not already enabled.

Step 3: Firebase initialized in your main function.

Step 4: Implement the sign-in logic

## Installation

1. Add the latest version of package to your pubspec.yaml (and run`dart pub get`):
```yaml
dependencies:
  firebase_sign_in: ^0.0.15
```
2. Import the package and use it in your Flutter App.
```dart
import 'package:firebase_sign_in/sociallogin.dart';
```
