import 'package:firebase_core/firebase_core.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'UserDetail.dart';

enum SignupType { google, apple, other }

class FirebaseSetup {
  late GoogleSignIn _googleSignIn;
  FirebaseSetup({required List<String> scopes}) {
    _googleSignIn = GoogleSignIn(
      scopes: scopes,
    );
  }

  static Future<void> initialize({required List<SignupType> types}) async {
    for (int i = 0; i < types.length - 1; i++) {
      switch (types[i]) {
        case SignupType.google:
          await Firebase.initializeApp();
        case SignupType.apple:
          break;
        default:
          break;
      }
    };
  }

  Future<UserDetails?> signin({required SignupType type}) async {
    switch (type) {
      case SignupType.google:
        return await _googleSignin();
      case SignupType.apple:
        break;
      default:
        break;
    }
    return null;
  }

  Future<UserDetails?> _googleSignin() async {
    try {
      final account = await _googleSignIn.signIn();
      // Handle the signed-in user account
      final userDetail = UserDetails();
      userDetail.id = account?.id;
      userDetail.userName = account?.displayName;
      userDetail.email = account?.email;
      userDetail.profileUrl = account?.photoUrl;
      userDetail.serverAuthCode = account?.serverAuthCode;
      return userDetail;
    } catch (error) {
      print('Sign-In Error: $error');
      return null;
    }
  }

  signout({required SignupType type}) {
    switch (type) {
      case SignupType.google:
        _googleSignIn.signOut();
      default:
        return null;
    }
  }
}
