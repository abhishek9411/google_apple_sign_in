import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_sign_in/sociallogin.dart';

Future<void> main() async {
  FirebaseSetup.initialize(types: [SignupType.google,SignupType.apple]);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
      home: const GoogleSignin(),
    );
  }
}

class GoogleSignin extends StatefulWidget {
  const GoogleSignin({Key? key}) : super(key: key);

  @override
  State<GoogleSignin> createState() => _GoogleSigninState();
}

class _GoogleSigninState extends State<GoogleSignin> {

  FirebaseSetup firebaseSetup = FirebaseSetup(scopes: ['email']);

  String? username;
  String? email;
  String? profileUrl;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.teal,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Visibility(
                  visible: email?.isEmpty == false,
                  child: Column(
                    children: [
                      ClipOval(
                        child: Image.network(
                          profileUrl ?? '',
                          height: 100,
                          width: 100,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(username ?? '',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: 20,
                      ),
                      Text(email ?? '', style: TextStyle(fontSize: 16)),
                      SizedBox(
                        height: 50,
                      ),
                    ],
                  )),
              Visibility(
                visible: email?.isEmpty ?? true,
                child: InkWell(
                  onTap: () async {
                    final account = await firebaseSetup.signin(type: SignupType.google);
                    setState(() {
                      this.username = account?.userName;
                      this.email = account?.email;
                      this.profileUrl = account?.profileUrl;
                    });
                  },
                  child: Container(
                    height: 50,
                    width: 200,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/googleIcon.png',
                          height: 20,
                          width: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text('Sign In with Google')
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              TextButton(
                  onPressed: () {
                    firebaseSetup.signout(type: SignupType.google);
                    setState(() {
                      email = '';
                      username = '';
                      profileUrl = '';
                    });
                  },
                  child: Text('Logout'))
            ],
          ),
        ),
      ),
    );
  }
}
